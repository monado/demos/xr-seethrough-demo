# OpenXR Seethrough Demo for the HTC VIVE

A OpenGL OpenXR demo using V4L2 for video seethrough.

## Build

```
cmake . && make
```

## Example Usage

```
./seethrough -d /dev/video1 -f YUYV
```
