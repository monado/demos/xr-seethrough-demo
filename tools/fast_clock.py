#!/usr/bin/env python3

import signal
signal.signal(signal.SIGINT, signal.SIG_DFL)

from datetime import datetime

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

label_count = 12

class FastClock():
  def __init__(self):
  
    self.last_time = datetime.now()

    self.current_timelabel = 0

    win = Gtk.Window()
    win.set_default_size(200,100)
    win.set_title("Fast Clock")
    win.connect("delete-event", Gtk.main_quit)
    
    self.time_labels = []
    for i in range(0, label_count):
      self.time_labels.append(Gtk.Label())
    self.fps_label = Gtk.Label()
    
    grid = Gtk.Grid()
    grid.set_column_homogeneous(True)
    
    vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)
    
    for i in range(0, label_count):
      if i < label_count / 2:
        grid.attach(self.time_labels[i], 0, i, 1, 1)
      else:
        grid.attach(self.time_labels[i], 1, int(i - label_count/2), 1, 1)
      self.time_labels[i].set_text("%d" % i)

    vbox.pack_start(grid, True, True, 0)
    vbox.pack_start(self.fps_label, False, False, 10)

    win.add(vbox)
    
    handler_id = win.connect("draw", self.draw_callback, None)

    win.show_all()
    Gtk.main()

  def draw_callback(self, window, cairo_ctx, data):
    dt = datetime.now()
    diff = dt - self.last_time
    self.last_time = dt

    fps = 1000000.0 / diff.microseconds

    # this is not enough?!
    #last_label = (self.current_timelabel - 1) % label_count
    #self.time_labels[last_label].set_markup('<span font="32"> </span>')

    for i in range(0, label_count):
      if i != self.current_timelabel:
        self.time_labels[i].set_text(' ')
    self.time_labels[self.current_timelabel].set_text('%.2d:%.6d'
      % (dt.second, dt.microsecond))
      
    self.fps_label.set_text('FPS %.2f (%.2fms)'
      % (fps, diff.microseconds / 1000.0))
      
    self.current_timelabel += 1
    if self.current_timelabel == label_count:
      self.current_timelabel = 0

if __name__ == "__main__":
  FastClock()
