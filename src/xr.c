// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: MIT
/*!
 * @brief  OpenXR GL example code.
 * @author Christoph Haag <christoph.haag@collabora.com>
 * @author Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 */

#include "xr.h"

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdarg.h>

#include <X11/Xlib.h>

static const char* viewport_config_name = "/viewport_configuration/vr";

bool
xr_result(XrInstance instance, XrResult result, const char* format, ...)
{
	if (XR_SUCCEEDED(result))
		return true;

	char resultString[XR_MAX_RESULT_STRING_SIZE];
	xrResultToString(instance, result, resultString);

	size_t len1 = strlen(format);
	size_t len2 = strlen(resultString) + 1;
	char formatRes[len1 + len2 + 4]; // + " []\n"
	sprintf(formatRes, "%s [%s]\n", format, resultString);

	va_list args;
	va_start(args, format);
	vprintf(formatRes, args);
	va_end(args);
	return false;
}

bool
is_extension_supported(char* name, XrExtensionProperties* props, uint32_t count)
{
	for (uint32_t i = 0; i < count; i++)
		if (!strcmp(name, props[i].extensionName))
			return true;
	return false;
}

static bool
_check_gl_extension()
{
	XrResult result;
	uint32_t instanceExtensionCount = 0;
	result = xrEnumerateInstanceExtensionProperties(
	    NULL, 0, &instanceExtensionCount, NULL);

	if (!xr_result(NULL, result,
	               "Failed to enumerate number of instance extension properties"))
		return false;

	XrExtensionProperties instanceExtensionProperties[instanceExtensionCount];
	for (uint16_t i = 0; i < instanceExtensionCount; i++)
		instanceExtensionProperties[i] = (XrExtensionProperties){
		    .type = XR_TYPE_EXTENSION_PROPERTIES,
		};

	result = xrEnumerateInstanceExtensionProperties(NULL, instanceExtensionCount,
	                                                &instanceExtensionCount,
	                                                instanceExtensionProperties);
	if (!xr_result(NULL, result, "Failed to enumerate extension properties"))
		return false;

	result = is_extension_supported(XR_KHR_OPENGL_ENABLE_EXTENSION_NAME,
	                                instanceExtensionProperties,
	                                instanceExtensionCount);
	if (!xr_result(NULL, result,
	               "Runtime does not support required instance extension %s\n",
	               XR_KHR_OPENGL_ENABLE_EXTENSION_NAME))
		return false;

	return true;
}

static bool
_enumerate_api_layers()
{
	bool lunargApiDumpSupported = false;
	bool lunargValidationSupported = false;
	uint32_t apiLayerCount;
	xrEnumerateApiLayerProperties(0, &apiLayerCount, NULL);

	XrApiLayerProperties apiLayerProperties[apiLayerCount];
	memset(apiLayerProperties, 0, apiLayerCount * sizeof(XrApiLayerProperties));

	for (uint32_t i = 0; i < apiLayerCount; i++) {
		apiLayerProperties[i].type = XR_TYPE_API_LAYER_PROPERTIES;
	}
	xrEnumerateApiLayerProperties(apiLayerCount, &apiLayerCount,
	                              apiLayerProperties);

	for (uint32_t i = 0; i < apiLayerCount; i++) {
		if (strcmp(apiLayerProperties->layerName, "XR_APILAYER_LUNARG_api_dump") ==
		    0) {
			lunargApiDumpSupported = true;
		} else if (strcmp(apiLayerProperties->layerName,
		                  "XR_APILAYER_LUNARG_core_validation") == 0) {
			lunargValidationSupported = true;
		}
	}

	return true;
}

static bool
_create_instance(xr_example* self)
{
	const char* const enabledExtensions[] = {XR_KHR_OPENGL_ENABLE_EXTENSION_NAME};

	XrInstanceCreateInfo instanceCreateInfo = {
	    .type = XR_TYPE_INSTANCE_CREATE_INFO,
	    .createFlags = 0,
	    .enabledExtensionCount = 1,
	    .enabledExtensionNames = enabledExtensions,
	    .enabledApiLayerCount = 0,
	    .applicationInfo =
	        {
	            .applicationName = "Seethrough Demo",
	            .engineName = "OpenGL",
	            .applicationVersion = 1,
	            .engineVersion = 1,
	            .apiVersion = XR_CURRENT_API_VERSION,
	        },
	};

	XrResult result;
	result = xrCreateInstance(&instanceCreateInfo, &self->instance);
	if (!xr_result(NULL, result, "Failed to create XR instance."))
		return false;

	return true;
}

static bool
_create_system(xr_example* self)
{
	XrPath vrConfigName;
	XrResult result;
	result = xrStringToPath(self->instance, viewport_config_name, &vrConfigName);
	xr_result(self->instance, result,
	          "failed to get viewport configuration name");

	XrSystemGetInfo systemGetInfo = {
	    .type = XR_TYPE_SYSTEM_GET_INFO,
	    .formFactor = XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY,
	};

	printf("Get system for vrconfig %lu\n", vrConfigName);

	result = xrGetSystem(self->instance, &systemGetInfo, &self->system_id);
	if (!xr_result(self->instance, result,
	               "Failed to get system for %s viewport configuration.",
	               viewport_config_name))
		return false;

	XrSystemProperties systemProperties = {
	    .type = XR_TYPE_SYSTEM_PROPERTIES,
	    .graphicsProperties = {0},
	    .trackingProperties = {0},
	};

	result =
	    xrGetSystemProperties(self->instance, self->system_id, &systemProperties);
	if (!xr_result(self->instance, result, "Failed to get System properties"))
		return false;

	return true;
}

static bool
_set_up_views(xr_example* self)
{
	uint32_t viewConfigurationCount;
	XrResult result;
	result = xrEnumerateViewConfigurations(self->instance, self->system_id, 0,
	                                       &viewConfigurationCount, NULL);
	if (!xr_result(self->instance, result,
	               "Failed to get view configuration count"))
		return false;

	XrViewConfigurationType viewConfigurations[viewConfigurationCount];
	result = xrEnumerateViewConfigurations(
	    self->instance, self->system_id, viewConfigurationCount,
	    &viewConfigurationCount, viewConfigurations);
	if (!xr_result(self->instance, result,
	               "Failed to enumerate view configurations!"))
		return false;

	self->view_config_type = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;
	XrViewConfigurationType optionalSecondaryViewConfigType =
	    XR_VIEW_CONFIGURATION_TYPE_PRIMARY_MONO;

	/* if struct (more specifically .type) is still 0 after searching, then
	 we have not found the config. This way we don't need to set a bool
	 found to true. */
	XrViewConfigurationProperties requiredViewConfigProperties = {0};
	XrViewConfigurationProperties secondaryViewConfigProperties = {0};

	for (uint32_t i = 0; i < viewConfigurationCount; ++i) {
		XrViewConfigurationProperties properties = {
		    .type = XR_TYPE_VIEW_CONFIGURATION_PROPERTIES,
		};

		result = xrGetViewConfigurationProperties(
		    self->instance, self->system_id, viewConfigurations[i], &properties);
		if (!xr_result(self->instance, result,
		               "Failed to get view configuration info %d!", i))
			return false;

		if (viewConfigurations[i] == self->view_config_type &&
		    properties.viewConfigurationType == self->view_config_type) {
			requiredViewConfigProperties = properties;
		} else if (viewConfigurations[i] == optionalSecondaryViewConfigType &&
		           properties.viewConfigurationType ==
		               optionalSecondaryViewConfigType) {
			secondaryViewConfigProperties = properties;
		}
	}
	if (requiredViewConfigProperties.type !=
	    XR_TYPE_VIEW_CONFIGURATION_PROPERTIES) {
		printf(
		    "Couldn't get required VR View Configuration %s from "
		    "Runtime!\n",
		    viewport_config_name);
		return false;
	}

	result = xrEnumerateViewConfigurationViews(self->instance, self->system_id,
	                                           self->view_config_type, 0,
	                                           &self->view_count, NULL);
	if (!xr_result(self->instance, result,
	               "Failed to get view configuration view count!"))
		return false;

	self->configuration_views =
	    malloc(sizeof(XrViewConfigurationView) * self->view_count);

	result = xrEnumerateViewConfigurationViews(
	    self->instance, self->system_id, self->view_config_type, self->view_count,
	    &self->view_count, self->configuration_views);
	if (!xr_result(self->instance, result,
	               "Failed to enumerate view configuration views!"))
		return false;

	uint32_t secondaryViewConfigurationViewCount = 0;
	if (secondaryViewConfigProperties.type ==
	    XR_TYPE_VIEW_CONFIGURATION_PROPERTIES) {

		result = xrEnumerateViewConfigurationViews(
		    self->instance, self->system_id, optionalSecondaryViewConfigType, 0,
		    &secondaryViewConfigurationViewCount, NULL);
		if (!xr_result(self->instance, result,
		               "Failed to get view configuration view count!"))
			return false;
	}

	if (secondaryViewConfigProperties.type ==
	    XR_TYPE_VIEW_CONFIGURATION_PROPERTIES) {
		result = xrEnumerateViewConfigurationViews(
		    self->instance, self->system_id, optionalSecondaryViewConfigType,
		    secondaryViewConfigurationViewCount,
		    &secondaryViewConfigurationViewCount, self->configuration_views);
		if (!xr_result(self->instance, result,
		               "Failed to enumerate view configuration views!"))
			return false;
	}

	return true;
}

static bool
_check_graphics_api_support(xr_example* self)
{
	PFN_xrGetOpenGLGraphicsRequirementsKHR xrGetOpenGLGraphicsRequirementsKHR;
	XrResult result =
		xrGetInstanceProcAddr(self->instance,
							  "xrGetOpenGLGraphicsRequirementsKHR",
							  (PFN_xrVoidFunction *)&xrGetOpenGLGraphicsRequirementsKHR);
	if (!xr_result(self->instance, result,
	               "Failed to get function pointer to "
				   "xrGetOpenGLGraphicsRequirementsKHR"))
		return false;

	XrGraphicsRequirementsOpenGLKHR opengl_reqs = {
	    .type = XR_TYPE_GRAPHICS_REQUIREMENTS_OPENGL_KHR,
	};
	result = xrGetOpenGLGraphicsRequirementsKHR(self->instance, self->system_id,
	                                            &opengl_reqs);
	if (!xr_result(self->instance, result,
	               "Failed to get OpenGL graphics requirements!"))
		return false;

	XrVersion desired_opengl_version = XR_MAKE_VERSION(4, 5, 0);
	printf(
	    "OpenGL version supported by runtime: min=%d.%d, max=%d.%d, "
	    "want=%d.%d\n",
	    XR_VERSION_MAJOR(opengl_reqs.minApiVersionSupported),
	    XR_VERSION_MINOR(opengl_reqs.minApiVersionSupported),
	    XR_VERSION_MAJOR(opengl_reqs.maxApiVersionSupported),
	    XR_VERSION_MINOR(opengl_reqs.maxApiVersionSupported),
	    XR_VERSION_MAJOR(desired_opengl_version),
	    XR_VERSION_MINOR(desired_opengl_version));

	if (desired_opengl_version > opengl_reqs.maxApiVersionSupported ||
	    desired_opengl_version < opengl_reqs.minApiVersionSupported) {
		printf("Runtime does not support requested OpenGL version.\n");
		return false;
	}
	return true;
}

static bool
_create_session(xr_example* self)
{
	XrSessionCreateInfo session_create_info = {
	    .type = XR_TYPE_SESSION_CREATE_INFO,
	    .next = &self->graphics_binding_gl,
	    .systemId = self->system_id,
	};

	XrResult result =
	    xrCreateSession(self->instance, &session_create_info, &self->session);
	if (!xr_result(self->instance, result, "Failed to create session"))
		return false;
	return true;
}

static bool
_check_supported_spaces(xr_example* self)
{
	uint32_t referenceSpacesCount;
	XrResult result =
	    xrEnumerateReferenceSpaces(self->session, 0, &referenceSpacesCount, NULL);
	if (!xr_result(self->instance, result,
	               "Getting number of reference spaces failed!"))
		return false;

	XrReferenceSpaceType referenceSpaces[referenceSpacesCount];
	result = xrEnumerateReferenceSpaces(self->session, referenceSpacesCount,
	                                    &referenceSpacesCount, referenceSpaces);
	if (!xr_result(self->instance, result,
	               "Enumerating reference spaces failed!"))
		return false;

	bool localSpaceSupported = false;
	printf("Enumerated %d reference spaces: ", referenceSpacesCount);
	for (uint32_t i = 0; i < referenceSpacesCount; i++) {
		if (referenceSpaces[i] == XR_REFERENCE_SPACE_TYPE_LOCAL) {
			localSpaceSupported = true;
		}
	}

	if (!localSpaceSupported) {
		fprintf(stderr, "XR_REFERENCE_SPACE_TYPE_LOCAL unsupported.\n");
		return false;
	}

	XrPosef identity = {
	    .orientation = {.x = 0, .y = 0, .z = 0, .w = 1.0},
	    .position = {.x = 0, .y = 0, .z = 0},
	};

	XrReferenceSpaceCreateInfo referenceSpaceCreateInfo = {
	    .type = XR_TYPE_REFERENCE_SPACE_CREATE_INFO,
	    .referenceSpaceType = XR_REFERENCE_SPACE_TYPE_LOCAL,
	    .poseInReferenceSpace = identity,
	};

	result = xrCreateReferenceSpace(self->session, &referenceSpaceCreateInfo,
	                                &self->local_space);
	if (!xr_result(self->instance, result, "Failed to create local space!"))
		return false;

	return true;
}

static bool
_begin_session(xr_example* self)
{
	XrSessionBeginInfo sessionBeginInfo = {
	    .type = XR_TYPE_SESSION_BEGIN_INFO,
	    .primaryViewConfigurationType = self->view_config_type,
	};
	XrResult result = xrBeginSession(self->session, &sessionBeginInfo);
	if (!xr_result(self->instance, result, "Failed to begin session!"))
		return false;

	return true;
}

static bool
_create_swapchains(xr_example* self)
{
	XrResult result;
	uint32_t swapchainFormatCount;
	result = xrEnumerateSwapchainFormats(self->session, 0, &swapchainFormatCount,
	                                     NULL);
	if (!xr_result(self->instance, result,
	               "Failed to get number of supported swapchain formats"))
		return false;

	int64_t swapchainFormats[swapchainFormatCount];
	result = xrEnumerateSwapchainFormats(self->session, swapchainFormatCount,
	                                     &swapchainFormatCount, swapchainFormats);
	if (!xr_result(self->instance, result,
	               "Failed to enumerate swapchain formats"))
		return false;

	/* First create swapchains and query the length for each swapchain. */
	self->swapchains = malloc(sizeof(XrSwapchain) * self->view_count);

	uint32_t swapchainLength[self->view_count];

	for (uint32_t i = 0; i < self->view_count; i++) {
		XrSwapchainCreateInfo swapchainCreateInfo = {
		    .type = XR_TYPE_SWAPCHAIN_CREATE_INFO,
		    .usageFlags = XR_SWAPCHAIN_USAGE_SAMPLED_BIT |
		                  XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT,
		    .createFlags = 0,
		    // just use the first enumerated format
		    .format = swapchainFormats[0],
		    .sampleCount = 1,
		    .width = self->configuration_views[i].recommendedImageRectWidth,
		    .height = self->configuration_views[i].recommendedImageRectHeight,
		    .faceCount = 1,
		    .arraySize = 1,
		    .mipCount = 1,
		};

		result = xrCreateSwapchain(self->session, &swapchainCreateInfo,
		                           &self->swapchains[i]);
		if (!xr_result(self->instance, result, "Failed to create swapchain %d!", i))
			return false;

		result = xrEnumerateSwapchainImages(self->swapchains[i], 0,
		                                    &swapchainLength[i], NULL);
		if (!xr_result(self->instance, result, "Failed to enumerate swapchains"))
			return false;
	}

	// most likely all swapchains have the same length, but let's not fail
	// if they are not
	uint32_t maxSwapchainLength = 0;
	for (uint32_t i = 0; i < self->view_count; i++) {
		if (swapchainLength[i] > maxSwapchainLength) {
			maxSwapchainLength = swapchainLength[i];
		}
	}

	self->images = malloc(sizeof(XrSwapchainImageOpenGLKHR*) * self->view_count);
	for (uint32_t i = 0; i < self->view_count; i++) {
		self->images[i] =
		    malloc(sizeof(XrSwapchainImageOpenGLKHR) * maxSwapchainLength);
		for (uint32_t j = 0; j < maxSwapchainLength; j++) {
			self->images[i][j].type = XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_KHR;
			self->images[i][j].next = NULL;
		}
	}

	self->framebuffers = malloc(sizeof(GLuint*) * self->view_count);
	for (uint32_t i = 0; i < self->view_count; i++)
		self->framebuffers[i] = malloc(sizeof(GLuint) * maxSwapchainLength);

	for (uint32_t i = 0; i < self->view_count; i++) {
		result = xrEnumerateSwapchainImages(
		    self->swapchains[i], swapchainLength[i], &swapchainLength[i],
		    (XrSwapchainImageBaseHeader*)self->images[i]);
		if (!xr_result(self->instance, result, "Failed to enumerate swapchains"))
			return false;

		glGenFramebuffers((GLsizei)swapchainLength[i], self->framebuffers[i]);
	}

	return true;
}

static void
_create_projection_views(xr_example* self)
{
	self->projection_views =
	    malloc(sizeof(XrCompositionLayerProjectionView) * self->view_count);

	for (uint32_t i = 0; i < self->view_count; i++)
		self->projection_views[i] = (XrCompositionLayerProjectionView){
		    .type = XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW,
		    .subImage =
		        {
		            .swapchain = self->swapchains[i],
		            .imageRect =
		                {
		                    .extent =
		                        {
		                            .width = (int32_t)self->configuration_views[i]
		                                         .recommendedImageRectWidth,
		                            .height = (int32_t)self->configuration_views[i]
		                                          .recommendedImageRectHeight,
		                        },
		                },
		        },
		};
}

bool
xr_begin_frame(xr_example* self)
{
	XrResult result;

	XrEventDataBuffer runtimeEvent = {
	    .type = XR_TYPE_EVENT_DATA_BUFFER,
	    .next = NULL,
	};

	self->frameState = (XrFrameState){
	    .type = XR_TYPE_FRAME_STATE,
	};
	XrFrameWaitInfo frameWaitInfo = {
	    .type = XR_TYPE_FRAME_WAIT_INFO,
	};
	result = xrWaitFrame(self->session, &frameWaitInfo, &self->frameState);
	if (!xr_result(self->instance, result,
	               "xrWaitFrame() was not successful, exiting..."))
		return false;

	XrResult pollResult = xrPollEvent(self->instance, &runtimeEvent);
	if (pollResult == XR_SUCCESS) {
		switch (runtimeEvent.type) {
		case XR_TYPE_EVENT_DATA_SESSION_STATE_CHANGED: {
			printf("EVENT: session state changed ");
			XrEventDataSessionStateChanged* event =
			    (XrEventDataSessionStateChanged*)&runtimeEvent;
			XrSessionState state = event->state;
			self->is_visible = event->state <= XR_SESSION_STATE_FOCUSED;
			printf("to %d. Visible: %d", state, self->is_visible);
			if (event->state >= XR_SESSION_STATE_STOPPING) { // TODO
				self->is_runnting = false;
			}
			printf("\n");
			break;
		}
		default: break;
		}
	} else if (pollResult == XR_EVENT_UNAVAILABLE) {
		// this is the usual case
	} else {
		printf("Failed to poll events!\n");
		return false;
	}
	if (!self->is_visible)
		return false;

	// --- Create projection matrices and view matrices for each eye
	XrViewLocateInfo viewLocateInfo = {
	    .type = XR_TYPE_VIEW_LOCATE_INFO,
	    .displayTime = self->frameState.predictedDisplayTime,
	    .space = self->local_space,
	};

	self->views = malloc(sizeof(XrView) * self->view_count);
	for (uint32_t i = 0; i < self->view_count; i++) {
		self->views[i].type = XR_TYPE_VIEW;
	};

	XrViewState viewState = {
	    .type = XR_TYPE_VIEW_STATE,
	};
	uint32_t viewCountOutput;
	result = xrLocateViews(self->session, &viewLocateInfo, &viewState,
	                       self->view_count, &viewCountOutput, self->views);
	if (!xr_result(self->instance, result, "Could not locate views"))
		return false;

	// --- Begin frame
	XrFrameBeginInfo frameBeginInfo = {
	    .type = XR_TYPE_FRAME_BEGIN_INFO,
	};

	result = xrBeginFrame(self->session, &frameBeginInfo);
	if (!xr_result(self->instance, result, "failed to begin frame!"))
		return false;

	return true;
}

bool
xr_aquire_swapchain(xr_example* self, uint32_t i, uint32_t* buffer_index)
{
	XrResult result;

	XrSwapchainImageAcquireInfo swapchainImageAcquireInfo = {
	    .type = XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO,
	};

	result = xrAcquireSwapchainImage(self->swapchains[i],
	                                 &swapchainImageAcquireInfo, buffer_index);
	if (!xr_result(self->instance, result, "failed to acquire swapchain image!"))
		return false;

	XrSwapchainImageWaitInfo swapchainImageWaitInfo = {
	    .type = XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO,
	    .timeout = INT64_MAX,
	};
	result = xrWaitSwapchainImage(self->swapchains[i], &swapchainImageWaitInfo);
	if (!xr_result(self->instance, result, "failed to wait for swapchain image!"))
		return false;

	self->projection_views[i].pose = self->views[i].pose;
	self->projection_views[i].fov = self->views[i].fov;
	self->projection_views[i].subImage.imageArrayIndex = *buffer_index;

	return true;
}

bool
xr_release_swapchain(xr_example* self, uint32_t eye)
{
	XrSwapchainImageReleaseInfo swapchainImageReleaseInfo = {
	    .type = XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO,
	};
	XrResult result = xrReleaseSwapchainImage(self->swapchains[eye],
	                                          &swapchainImageReleaseInfo);
	if (!xr_result(self->instance, result, "failed to release swapchain image!"))
		return false;

	return true;
}

bool
xr_end_frame(xr_example* self)
{
	XrResult result;

	const XrCompositionLayerBaseHeader* const projectionlayers[1] = {
	    (const XrCompositionLayerBaseHeader* const) & self->projectionLayer};
	XrFrameEndInfo frameEndInfo = {
	    .type = XR_TYPE_FRAME_END_INFO,
	    .displayTime = self->frameState.predictedDisplayTime,
	    .layerCount = 1,
	    .layers = projectionlayers,
	    .environmentBlendMode = XR_ENVIRONMENT_BLEND_MODE_OPAQUE,
	};
	result = xrEndFrame(self->session, &frameEndInfo);
	if (!xr_result(self->instance, result, "failed to end frame!"))
		return false;

	free(self->views);

	return true;
}

void
xr_cleanup(xr_example* self)
{
	xrDestroySession(self->session);
	xrDestroyInstance(self->instance);
}

bool
xr_init(xr_example* self)
{
	if (!_check_gl_extension())
		return false;

	if (!_enumerate_api_layers())
		return false;

	if (!_create_instance(self))
		return false;

	if (!_create_system(self))
		return false;

	if (!_set_up_views(self))
		return false;

	if (!_check_graphics_api_support(self))
		return false;

	self->graphics_binding_gl = (XrGraphicsBindingOpenGLXlibKHR){
	    .type = XR_TYPE_GRAPHICS_BINDING_OPENGL_XLIB_KHR,
	    .xDisplay = XOpenDisplay(NULL),
	    .glxDrawable = glXGetCurrentDrawable(),
	    .glxContext = glXGetCurrentContext(),
	};

	if (!_create_session(self))
		return false;

	if (!_check_supported_spaces(self))
		return false;

	if (!_begin_session(self))
		return false;

	if (!_create_swapchains(self))
		return false;

	_create_projection_views(self);

	self->is_visible = true;
	self->is_runnting = true;

	self->projectionLayer = (XrCompositionLayerProjection){
	    .type = XR_TYPE_COMPOSITION_LAYER_PROJECTION,
	    .layerFlags = 0,
	    .space = self->local_space,
	    .viewCount = self->view_count,
	    .views = self->projection_views,
	};

	return true;
}
