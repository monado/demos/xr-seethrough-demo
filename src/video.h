// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: MIT
/*!
 * @brief  V4L2 streaming code header.
 * @author Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 */

#pragma once

#include <stddef.h>
#include <stdbool.h>
#include <linux/videodev2.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct video_buffer
{
	unsigned char *start;
	size_t length;
} video_buffer;

typedef struct video
{
	char *dev_name;
	int fd;
	video_buffer *buffers;
	unsigned int n_buffers;
} video;

bool
video_init(video *v, struct v4l2_format *fmt);

void
video_destroy(video *v);

bool
video_dequeue_buffer(video *v,
                     struct v4l2_buffer *buf,
                     unsigned char **out_buffer);

bool
video_queue_buffer(video *v, struct v4l2_buffer *buf);

bool
video_select(video *v);

void
video_create(video *v);

#ifdef __cplusplus
}
#endif
