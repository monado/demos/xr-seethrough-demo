// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: MIT
/*!
 * @brief  Video seethrough demo code.
 * @author Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 */

#include <getopt.h>
#include <string.h>

#include "video.h"
#include "renderer.h"

static int
read_frame(video *v, Renderer *gl)
{
	unsigned char *data;

	struct v4l2_buffer buf = {
	    .type = V4L2_BUF_TYPE_VIDEO_CAPTURE,
	    .memory = V4L2_MEMORY_MMAP,
	};

	if (!video_dequeue_buffer(v, &buf, &data))
		return false;

	gl->set_frame(data);

	if (!video_queue_buffer(v, &buf))
		return false;

	return true;
}

static void
capture_loop(video *v, Renderer *gl)
{
	while (!gl->should_close()) {
		if (!video_select(v))
			return;

		if (!read_frame(v, gl))
			return;
	}
}

static void
usage(video *v, FILE *fp, char **argv)
{
	fprintf(fp,
	        "Usage: %s [options]\n\n"
	        "Options:\n"
	        "-d | --device name   Video device name [%s]\n"
	        "-f | --format format Format name [YUYV, NV12] (default: YUYV)\n"
	        "-h | --help          Print this message\n"
	        "",
	        argv[0], v->dev_name);
}

static const char short_options[] = "d:f:h:";

static const struct option long_options[] = {
    {"device", required_argument, nullptr, 'd'},
    {"format", required_argument, nullptr, 'f'},
    {"help", no_argument, nullptr, 'h'},
    {nullptr, 0, nullptr, 0}};

int
main(int argc, char **argv)
{
	video v;
	video_create(&v);

	uint32_t selected_format = V4L2_PIX_FMT_YUYV;

	while (true) {
		int idx;
		int c = getopt_long(argc, argv, short_options, long_options, &idx);

		if (-1 == c)
			break;
		switch (c) {
		case 0: /* getopt_long() flag */ break;

		case 'd': v.dev_name = optarg; break;

		case 'f': {
			char *format_name = optarg;

			if (strcmp(format_name, "YUYV") == 0) {
				printf("Using format YUYV\n");
			} else if (strcmp(format_name, "NV12") == 0) {
				printf("Using format NV12\n");
				selected_format = V4L2_PIX_FMT_NV12;
			} else {
				printf("Unknown format %s. Using default format YUYV.\n", format_name);
			}
			break;
		}

		case 'h': usage(&v, stdout, argv); exit(EXIT_SUCCESS);
		default: usage(&v, stderr, argv); exit(EXIT_FAILURE);
		}
	}

	struct v4l2_format format = {
	    .type = V4L2_BUF_TYPE_VIDEO_CAPTURE,
	    .fmt =
	        {
	            .pix =
	                {
	                    .pixelformat = selected_format,
	                    .field = V4L2_FIELD_NONE,
	                },
	        },
	};

	/* Select VIVE 1 resolution for video format */
	switch (selected_format) {
	case V4L2_PIX_FMT_YUYV:
		format.fmt.pix.width = 612;
		format.fmt.pix.height = 460;
		break;
	case V4L2_PIX_FMT_NV12:
		format.fmt.pix.width = 480;
		format.fmt.pix.height = 360;
		break;
	}

	if (!video_init(&v, &format))
		return -1;

	Renderer gl;
	gl.init();
	gl.set_format(format.fmt.pix);

	capture_loop(&v, &gl);

	video_destroy(&v);

	return 0;
}
