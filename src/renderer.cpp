// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: MIT
/*!
 * @brief  Video seethrough GL rendering code.
 * @author Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 */

#include "renderer.h"

#include <GL/glext.h>

#define GLM_ENABLE_EXPERIMENTAL
#define GLM_FORCE_CTOR_INIT
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtc/matrix_transform.hpp>

static std::string
pixfmt2s(unsigned id)
{
	std::string pixfmt;
	pixfmt += static_cast<char>(id & 0x7f);
	pixfmt += static_cast<char>((id >> 8) & 0x7f);
	pixfmt += static_cast<char>((id >> 16) & 0x7f);
	pixfmt += static_cast<char>((id >> 24) & 0x7f);
	return pixfmt;
}

Renderer::Renderer() : texture_count(0), raw_frame(nullptr)
{
	video_transform[0].scale = 0.951631f;
	video_transform[0].translation = glm::vec3(0.084000f, -0.265001f, 0.419998f);
	video_transform[0].euler_angle =
	    glm::vec3(-0.009398f, -0.423050f, -0.014541f);
	update_transformation(0);

	video_transform[1].scale = 0.951631f;
	video_transform[1].translation = glm::vec3(-0.026000f, -0.256001f, 0.423999f);
	video_transform[1].euler_angle = glm::vec3(0.017602f, -0.520048f, 0.019459f);
	update_transformation(1);
}

Renderer::~Renderer()
{
	glDeleteTextures(texture_count, video_textures);
}

void
Renderer::set_frame(unsigned char *data)
{
	raw_frame = data;
	render_frame();
}

void
Renderer::set_format(v4l2_pix_format format)
{
	pix_format = format;

	printf("setting frame format to %s\n", pixfmt2s(format.pixelformat).c_str());

	switch (pix_format.pixelformat) {
	case V4L2_PIX_FMT_YUYV: init_yuy2(); break;

	case V4L2_PIX_FMT_NV12:
	default: init_nv12(); break;
	}

	decode_program.use();
	decode_program.set_i("texSampler", 0);
	decode_program.set_f("scale", undistortion_scale);

	init_video_vbo();
	vig::check_error("set_format");
}

bool
Renderer::format_supported(__u32 format)
{
	static const __u32 supported_fmts[] = {V4L2_PIX_FMT_YUYV, V4L2_PIX_FMT_NV12,
	                                       0};

	for (int i = 0; supported_fmts[i]; i++)
		if (supported_fmts[i] == format)
			return true;

	return false;
}

void
Renderer::update_transformation(int viewport_index)
{
	video_transformation *transform = &video_transform[viewport_index];

	float aspect = (float)eye_w / (float)eye_h;

	glm::mat4 rotation =
	    glm::eulerAngleYXZ(transform->euler_angle.x, transform->euler_angle.y,
	                       transform->euler_angle.z);

	// float fov_degrees = 110.38696554f;

	glm::mat4 projection = glm::perspective(fov_radians, aspect, 0.01f, 100.0f);

	glm::vec3 eye = glm::vec3(0.0f, 0.0f, 1.0f);

	glm::vec3 center = glm::vec3(0.0f, 0.0f, 0.0f);

	glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);

	glm::mat4 view = glm::lookAt(eye, center, up);

	// glm::mat4 projection = glm::ortho(-aspect, aspect, -1.0f, 1.0f);
	// glm::mat4 projection = glm::ortho(-2.1f, 2.1f, -2.0f, 2.0f, -2.0f, 2.0f);

	glm::vec3 scale_vec = glm::vec3(transform->scale, transform->scale, 1.0f);

	glm::mat4 scale = glm::scale(glm::mat4(), scale_vec);

	glm::mat4 translation = glm::translate(scale, transform->translation);

	mvp[viewport_index] = projection * view * translation * rotation;
}

void
Renderer::init_nv12()
{
	texture_count = 2;
	glGenTextures(texture_count, video_textures);

	glActiveTexture(GL_TEXTURE0);

	vig::set_texture_filter(video_textures[0], GL_NEAREST);
	vig::set_texture_wrap(video_textures[0], GL_CLAMP_TO_EDGE);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, (GLsizei)pix_format.width,
	             (GLsizei)pix_format.height, 0, GL_RED, GL_UNSIGNED_BYTE,
	             nullptr);

	glActiveTexture(GL_TEXTURE1);

	vig::set_texture_filter(video_textures[1], GL_NEAREST);
	vig::set_texture_wrap(video_textures[1], GL_CLAMP_TO_EDGE);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, (GLsizei)pix_format.width,
	             (GLsizei)pix_format.height / 2, 0, GL_RED, GL_UNSIGNED_BYTE,
	             nullptr);


	decode_program.load_vert_frag("gpu/transform.vert",
	                              "gpu/nv12_undistort.frag");

	decode_program.use();

	decode_program.set_f("texl_w", 1.0f / pix_format.width);

	glm::vec2 resolution = glm::vec2(pix_format.width, pix_format.height);
	decode_program.set_vec2("resolution", resolution);

	decode_program.set_i("ytex", 0);
	decode_program.set_i("uvtex", 1);

	vig::check_error("init_nv12");
}

void
Renderer::bind_nv12()
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, video_textures[0]);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, (GLsizei)pix_format.width,
	                (GLsizei)pix_format.height, GL_RED, GL_UNSIGNED_BYTE,
	                raw_frame);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, video_textures[1]);

	const void *pixels = nullptr;
	if (raw_frame)
		pixels = raw_frame + pix_format.width * pix_format.height;

	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, (GLsizei)pix_format.width,
	                (GLsizei)pix_format.height / 2, GL_RED, GL_UNSIGNED_BYTE,
	                pixels);

	vig::check_error("bind_nv12");
}

void
Renderer::init_yuy2()
{
	printf("shader_YUY2\n");

	texture_count = 1;
	glGenTextures(texture_count, video_textures);
	vig::set_texture_filter(video_textures[0], GL_NEAREST);
	vig::set_texture_wrap(video_textures[0], GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)pix_format.width / 2,
	             (GLsizei)pix_format.height, 0, GL_RGBA, GL_UNSIGNED_BYTE,
	             nullptr);

	decode_program.load_vert_frag("gpu/transform.vert",
	                              //"gpu/yuyv.frag"
	                              "gpu/yuyv_undistort.frag");
	decode_program.use();
	decode_program.set_f("texl_w", 1.0f / pix_format.width);
	glm::vec2 resolution = glm::vec2(pix_format.width, pix_format.height);
	decode_program.set_vec2("resolution", resolution);
	decode_program.set_i("tex", 0);

	vig::check_error("init_yuy2");
}

void
Renderer::bind_yuy2()
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, video_textures[0]);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, (GLsizei)pix_format.width / 2,
	                (GLsizei)pix_format.height, GL_RGBA, GL_UNSIGNED_BYTE,
	                raw_frame);
	vig::check_error("bind_yuy2");
}

void
Renderer::size_cb(GLFWwindow *window, int width, int height)
{
	(void)window;
	glViewport(0, 0, width, height);
}

int
Renderer::init_context()
{

	// glfw
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// glfw window
	window =
	    glfwCreateWindow(2 * eye_w, eye_h, "Seethrough Demo", nullptr, nullptr);
	if (window == nullptr) {
		printf("Failed to create GLFW window\n");
		glfwTerminate();
		return -1;
	}

	auto key_cb = [](GLFWwindow *w, int key, int scancode, int action, int mods) {
		(void)scancode;
		(void)mods;
		static_cast<Renderer *>(glfwGetWindowUserPointer(w))
		    ->key_callback(key, action);
	};
	glfwSetKeyCallback(window, key_cb);

	glfwSetWindowUserPointer(window, this);
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, this->size_cb);

	// gl3w
	if (gl3wInit()) {
		fprintf(stderr, "failed to initialize OpenGL\n");
		return -1;
	}

	if (!gl3wIsSupported(4, 5)) {
		fprintf(stderr, "OpenGL 4.5 not supported\n");
		return -1;
	}
	printf("OpenGL %s, GLSL %s\n", glGetString(GL_VERSION),
	       glGetString(GL_SHADING_LANGUAGE_VERSION));

	return 0;
}

void
Renderer::key_callback(int key, int action)
{
	if (key == GLFW_KEY_ESCAPE) {
		glfwSetWindowShouldClose(window, true);
	} else if (key == GLFW_KEY_P && action == GLFW_PRESS) {

		printf("distortion_scale: %f\n", undistortion_scale);
		printf("fov_radians: %f\n", fov_radians);

		printf("++++++++++++++ Left Eye +++++++++++++++++\n");
		printf("scale: %f\n", video_transform[0].scale);
		printf("translation: %f %f %f\n", video_transform[0].translation.x,
		       video_transform[0].translation.y, video_transform[0].translation.z);

		printf("yaw: %f\n", video_transform[0].euler_angle.x);
		printf("pitch: %f\n", video_transform[0].euler_angle.y);
		printf("roll: %f\n", video_transform[0].euler_angle.z);

		printf("++++++++++++++ Right Eye +++++++++++++++++\n");
		printf("scale: %f\n", video_transform[1].scale);
		printf("translation: %f %f %f\n", video_transform[1].translation.x,
		       video_transform[1].translation.y, video_transform[1].translation.z);

		printf("yaw: %f\n", video_transform[1].euler_angle.x);
		printf("pitch: %f\n", video_transform[1].euler_angle.y);
		printf("roll: %f\n", video_transform[1].euler_angle.z);
	} else if (key == GLFW_KEY_TAB && action == GLFW_PRESS) {
		if (current_eye == 0)
			current_eye = 1;
		else
			current_eye = 0;
	} else if (key == GLFW_KEY_SPACE && action == GLFW_PRESS) {
		use_hmd_distortion = !use_hmd_distortion;
	} else if (action == GLFW_PRESS || action == GLFW_REPEAT) {
		switch (key) {
		case GLFW_KEY_LEFT:
			video_transform[current_eye].translation.x -= translation_step;
			break;
		case GLFW_KEY_RIGHT:
			video_transform[current_eye].translation.x += translation_step;
			break;
		case GLFW_KEY_UP:
			video_transform[current_eye].translation.y += translation_step;
			break;
		case GLFW_KEY_DOWN:
			video_transform[current_eye].translation.y -= translation_step;
			break;
		case GLFW_KEY_S:
			fov_radians -= fov_step;
			update_transformation(0);
			update_transformation(1);
			break;
		case GLFW_KEY_D:
			fov_radians += fov_step;
			update_transformation(0);
			update_transformation(1);
			break;
		case GLFW_KEY_X:
			video_transform[current_eye].translation.z -= translation_step;
			break;
		case GLFW_KEY_C:
			video_transform[current_eye].translation.z += translation_step;
			break;
		case GLFW_KEY_Y:
			video_transform[current_eye].euler_angle.x += rotation_step;
			break;
		case GLFW_KEY_T:
			video_transform[current_eye].euler_angle.x -= rotation_step;
			break;
		case GLFW_KEY_H:
			video_transform[current_eye].euler_angle.y += rotation_step;
			break;
		case GLFW_KEY_G:
			video_transform[current_eye].euler_angle.y -= rotation_step;
			break;
		case GLFW_KEY_N:
			video_transform[current_eye].euler_angle.z += rotation_step;
			break;
		case GLFW_KEY_B:
			video_transform[current_eye].euler_angle.z -= rotation_step;
			break;
		default: break;
		}

		update_transformation(current_eye);
	}
}

void
Renderer::render_into_stereo_fb()
{
	switch (pix_format.pixelformat) {
	case V4L2_PIX_FMT_YUYV: bind_yuy2(); break;
	case V4L2_PIX_FMT_NV12: bind_nv12(); break;
	default: return;
	}

	xr_begin_frame(&xr);

	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);

	uint32_t buffer_index;

	if (!xr_aquire_swapchain(&xr, 0, &buffer_index)) {
		fprintf(stderr, "Could not aquire xr swapchain\n");
		return;
	}

	// Draw scene into framebuffer.
	glBindFramebuffer(GL_FRAMEBUFFER, xr.framebuffers[0][buffer_index]);

	GLsizei w = (GLsizei)xr.configuration_views[0].recommendedImageRectWidth;
	GLsizei h = (GLsizei)xr.configuration_views[0].recommendedImageRectHeight;

	glViewport(0, 0, w, h);
	glScissor(0, 0, w, h);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
	                       xr.images[0][buffer_index].image, 0);

	glClear(GL_COLOR_BUFFER_BIT);

	// draw
	decode_program.use();

	update_transformation(0);

	decode_program.set_mat4("mvp", mvp[0]);

	video_mesh.draw();

	if (!xr_release_swapchain(&xr, 0)) {
		fprintf(stderr, "Could not release xr swapchain\n");
		return;
	}

	if (!xr_aquire_swapchain(&xr, 1, &buffer_index)) {
		fprintf(stderr, "Could not aquire xr swapchain\n");
		return;
	}

	// Draw scene into framebuffer.
	glBindFramebuffer(GL_FRAMEBUFFER, xr.framebuffers[1][buffer_index]);

	w = (GLsizei)xr.configuration_views[1].recommendedImageRectWidth;
	h = (GLsizei)xr.configuration_views[1].recommendedImageRectHeight;

	glViewport(0, 0, w, h);
	glScissor(0, 0, w, h);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
	                       xr.images[1][buffer_index].image, 0);

	glClear(GL_COLOR_BUFFER_BIT);

	// draw
	decode_program.set_mat4("mvp", mvp[1]);

	video_mesh.draw();

	if (!xr_release_swapchain(&xr, 1)) {
		fprintf(stderr, "Could not release xr swapchain\n");
		return;
	}

	if (!xr_end_frame(&xr)) {
		fprintf(stderr, "Could not end xr frame\n");
	}

	// Clean up common draw state
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void
Renderer::render_stereo_fb()
{
	glViewport(0, 0, eye_w * 2, eye_h);

	glActiveTexture(GL_TEXTURE2);

	texture_program.use();

	glBindTexture(GL_TEXTURE_2D, xr.framebuffers[0][0]);
	left_eye_mesh.draw();

	glBindTexture(GL_TEXTURE_2D, xr.framebuffers[1][0]);
	right_eye_mesh.draw();

	vig::check_error("render_stereo_fb");
}

void
Renderer::render_frame()
{
	render_into_stereo_fb();

	render_stereo_fb();

	glfwSwapBuffers(window);
	glfwPollEvents();

	vig::check_error("render_frame");
}

void
Renderer::render_loop()
{
	while (!glfwWindowShouldClose(window))
		render_frame();
}

void
Renderer::init_shaders()
{
	texture_program.load_vert_frag("gpu/uv.vert", "gpu/texture.frag");
	texture_program.use();
	texture_program.set_i("tex", 2);
}

void
Renderer::init_video_vbo()
{
	float aspect = static_cast<float>(pix_format.width) /
	               static_cast<float>(pix_format.height);

	GLfloat vertices[] = {-aspect, 1.0, aspect, 1.0, aspect, -1.0, -aspect, -1.0};
	video_mesh.init_uv_plane(vertices);
	video_mesh.mesh_bind_shader(decode_program.handle);
	video_mesh.unbind();
}

void
Renderer::init_vbos()
{
	GLfloat left_positions[] = {-1.0, 1.0, 0.0, 1.0, 0.0, -1.0, -1.0, -1.0};

	left_eye_mesh.init_uv_plane(left_positions);
	left_eye_mesh.mesh_bind_shader(texture_program.handle);
	left_eye_mesh.unbind();

	GLfloat right_positions[] = {0.0, 1.0, 1.0, 1.0, 1.0, -1.0, 0.0, -1.0};

	right_eye_mesh.init_uv_plane(right_positions);
	right_eye_mesh.mesh_bind_shader(texture_program.handle);
	right_eye_mesh.unbind();
}

int
Renderer::init()
{
	if (-1 == init_context()) {
		printf("gl context creation failed.\n");
		return -1;
	}

	xr_init(&xr);

	GLint res = 0;
	glGetIntegerv(GL_FRAMEBUFFER_SRGB_CAPABLE_EXT, &res);
	printf("GL_FRAMEBUFFER_SRGB_CAPABLE_EXT: %d\n", res);
	if (res)
		glEnable(GL_FRAMEBUFFER_SRGB);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	vig::check_error("init");

	init_shaders();
	init_vbos();

	return 0;
}
