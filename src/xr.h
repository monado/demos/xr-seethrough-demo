// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: MIT
/*!
 * @brief  OpenXR GL example code header.
 * @author Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * @author Christoph Haag <christoph.haag@collabora.com>
 */

#pragma once

#include <stdbool.h>

#define GL_GLEXT_PROTOTYPES 1
#define GL3_PROTOTYPES 1
#include <GL/glcorearb.h>
#include <GL/glx.h>
#include "GL/gl3w.h"

#define XR_USE_PLATFORM_XLIB 1
#define XR_USE_GRAPHICS_API_OPENGL 1
#include <openxr/openxr.h>
#include <openxr/openxr_platform.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct xr_example
{
	XrInstance instance;
	XrSession session;
	XrSpace local_space;
	XrSystemId system_id;
	XrViewConfigurationType view_config_type;

	XrSwapchain* swapchains;
	XrCompositionLayerProjectionView* projection_views;
	XrViewConfigurationView* configuration_views;

	XrGraphicsBindingOpenGLXlibKHR graphics_binding_gl;

	XrSwapchainImageOpenGLKHR** images;

	GLuint** framebuffers;

	uint32_t view_count;

	bool is_visible;
	bool is_runnting;

	// render state
	XrCompositionLayerProjection projectionLayer;
	XrFrameState frameState;
	XrView* views;

} xr_example;

bool
xr_init(xr_example* self);

void
xr_cleanup(xr_example* self);

bool
xr_begin_frame(xr_example* self);

bool
xr_aquire_swapchain(xr_example* self, uint32_t i, uint32_t* buffer_index);

bool
xr_release_swapchain(xr_example* self, uint32_t eye);

bool
xr_end_frame(xr_example* self);

#ifdef __cplusplus
}
#endif
