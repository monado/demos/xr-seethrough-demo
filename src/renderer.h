// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: MIT
/*!
 * @brief  Video seethrough GL rendering code header.
 * @author Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 */

#pragma once

#include <linux/videodev2.h>
#include <GL/glcorearb.h>

#include <GLFW/glfw3.h>

#include "vitamin-g.h"

#include "xr.h"

#define MAX_TEXTURES_NEEDED 2

typedef struct video_transformation
{
	float scale;
	glm::vec3 translation;
	glm::vec3 euler_angle; // yaw, pitch, roll
} video_transformation;

class Renderer
{
	GLFWwindow *window;
	vig::Shader decode_program;
	vig::Shader texture_program;

	vig::Mesh video_mesh;
	vig::Mesh left_eye_mesh;
	vig::Mesh right_eye_mesh;

	xr_example xr;

	int eye_w = 1080;
	int eye_h = 1200;

	// interactive
	float translation_step = 0.001f;
	float plane_scale_step = 0.001f;
	float distortion_scale_step = 0.001f;
	float rotation_step = 0.001f;
	float fov_step = 0.001f;

	video_transformation video_transform[2];

	glm::mat4 mvp[2];

	int current_eye = 0;

	bool use_hmd_distortion = true;

	float undistortion_scale = 0.506999f;

public:
	Renderer();
	~Renderer();

	void
	set_frame(unsigned char *data);
	void
	set_format(v4l2_pix_format format);
	bool
	format_supported(__u32 format);

private:
	// Colorspace conversion shaders
	void
	init_nv12();
	void
	init_yuy2();

	// Colorspace conversion render
	void
	bind_yuy2();
	void
	bind_nv12();

	void
	key_callback(int key, int action);

	void
	render_into_stereo_fb();
	void
	render_stereo_fb();

	int texture_count;
	v4l2_pix_format pix_format;
	GLuint video_textures[MAX_TEXTURES_NEEDED];
	unsigned char *raw_frame;

	float fov_radians = 1.926616f;

	// glfw
	int
	init_context();

	void
	update_transformation(int viewport_index);

	void
	init_video_vbo();
	void
	render_frame();

	void
	init_vbos();
	void
	init_shaders();

public:
	void
	render_loop();
	int
	init();

	static void
	size_cb(GLFWwindow *window, int width, int height);

	bool
	should_close()
	{
		return glfwWindowShouldClose(window);
	}
};
