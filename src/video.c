// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: MIT
/*!
 * @brief  V4L2 streaming code.
 * @author Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 */

#include "video.h"

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <assert.h>

#include <sys/mman.h>
#include <sys/stat.h>

#include <libv4l2.h>

static void
print_err(const char *s)
{
	fprintf(stderr, "%s error %d, %s\n", s, errno, strerror(errno));
}

static bool
video_start_capturing(video *v)
{
	for (uint32_t i = 0; i < v->n_buffers; ++i) {
		struct v4l2_buffer buf = {
		    .type = V4L2_BUF_TYPE_VIDEO_CAPTURE,
		    .memory = V4L2_MEMORY_MMAP,
		    .index = i,
		};

		if (-1 == v4l2_ioctl(v->fd, VIDIOC_QBUF, &buf)) {
			print_err("VIDIOC_QBUF");
			return false;
		}
	}
	enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (-1 == v4l2_ioctl(v->fd, VIDIOC_STREAMON, &type)) {
		print_err("VIDIOC_STREAMON");
		return false;
	}

	return true;
}

static bool
video_init_mmap(video *v)
{
	struct v4l2_requestbuffers req = {
	    .count = 4,
	    .type = V4L2_BUF_TYPE_VIDEO_CAPTURE,
	    .memory = V4L2_MEMORY_MMAP,
	};

	if (-1 == v4l2_ioctl(v->fd, VIDIOC_REQBUFS, &req)) {
		if (EINVAL == errno) {
			fprintf(stderr, "'%s' does not support memory mapping.\n", v->dev_name);
			return false;
		} else {
			print_err("VIDIOC_REQBUFS");
			return false;
		}
	}

	if (req.count < 2) {
		fprintf(stderr, "Insufficient buffer memory on '%s'.\n", v->dev_name);
		return false;
	}

	v->buffers = (video_buffer *)calloc(req.count, sizeof(*v->buffers));

	if (!v->buffers) {
		fprintf(stderr, "Out of memory.\n");
		return false;
	}

	for (v->n_buffers = 0; v->n_buffers < req.count; ++v->n_buffers) {
		struct v4l2_buffer buf = {
		    .type = V4L2_BUF_TYPE_VIDEO_CAPTURE,
		    .memory = V4L2_MEMORY_MMAP,
		    .index = v->n_buffers,
		};

		if (-1 == v4l2_ioctl(v->fd, VIDIOC_QUERYBUF, &buf)) {
			print_err("VIDIOC_QUERYBUF");
			return false;
		}

		v->buffers[v->n_buffers].length = buf.length;
		v->buffers[v->n_buffers].start = (unsigned char *)mmap(
		    NULL /* start anywhere */, buf.length,
		    PROT_READ | PROT_WRITE /* required */, MAP_SHARED /* recommended */,
		    v->fd, buf.m.offset);

		if (MAP_FAILED == v->buffers[v->n_buffers].start) {
			print_err("mmap");
			return false;
		}
	}

	return true;
}

static void
video_uninit_device(video *v)
{
	free(v->buffers);
}

static bool
video_init_device(video *v, struct v4l2_format *fmt)
{
	struct v4l2_capability cap;

	if (-1 == v4l2_ioctl(v->fd, VIDIOC_QUERYCAP, &cap)) {
		if (EINVAL == errno) {
			fprintf(stderr, "'%s' is not a valid V4L2 device.\n", v->dev_name);
			return false;
		} else {
			print_err("VIDIOC_QUERYCAP");
			return false;
		}
	}

	if (!(cap.device_caps & V4L2_CAP_VIDEO_CAPTURE)) {
		fprintf(stderr, "'%s' is not a video capture device.\n", v->dev_name);
		return false;
	}

	if (-1 == v4l2_ioctl(v->fd, VIDIOC_S_FMT, fmt)) {
		print_err("VIDIOC_S_FMT");
		return false;
	}

	video_init_mmap(v);

	return true;
}

static void
video_close_device(video *v)
{
	if (-1 == close(v->fd))
		print_err("close");

	v->fd = -1;
}

static bool
video_open_device(video *v)
{
	struct stat st;

	if (-1 == stat(v->dev_name, &st)) {
		fprintf(stderr, "Cannot identify '%s': %d, %s\n", v->dev_name, errno,
		        strerror(errno));
		return false;
	}

	if (!S_ISCHR(st.st_mode)) {
		fprintf(stderr, "'%s' is not a valid device.\n", v->dev_name);
		return false;
	}

	v->fd = v4l2_open(v->dev_name, O_RDWR /* required */ | O_NONBLOCK);

	if (-1 == v->fd) {
		fprintf(stderr, "Cannot open '%s': %d, %s\n", v->dev_name, errno,
		        strerror(errno));
		return false;
	}

	return true;
}

void
video_create(video *v)
{
	v->dev_name = "/dev/video0";
	v->fd = -1;
}

bool
video_init(video *v, struct v4l2_format *fmt)
{
	if (!video_open_device(v))
		return false;
	if (!video_init_device(v, fmt))
		return false;
	if (!video_start_capturing(v))
		return false;

	return true;
}

void
video_destroy(video *v)
{
	video_uninit_device(v);
	video_close_device(v);
}

bool
video_dequeue_buffer(video *v,
                     struct v4l2_buffer *buf,
                     unsigned char **out_buffer)
{
	if (-1 == v4l2_ioctl(v->fd, VIDIOC_DQBUF, buf)) {
		print_err("VIDIOC_DQBUF");
		return false;
	}

	assert(buf->index < v->n_buffers);

	*out_buffer = v->buffers[buf->index].start;

	return true;
}

bool
video_queue_buffer(video *v, struct v4l2_buffer *buf)
{
	if (-1 == v4l2_ioctl(v->fd, VIDIOC_QBUF, buf)) {
		print_err("VIDIOC_QBUF");
		return false;
	}

	return true;
}

bool
video_select(video *v)
{
	fd_set fds;
	FD_ZERO(&fds);
	FD_SET(v->fd, &fds);

	struct timeval timeout = {
	    .tv_sec = 2,
	    .tv_usec = 0,
	};

	int ret = select(v->fd + 1, &fds, NULL, NULL, &timeout);

	if (ret == -1) {
		if (errno == EINTR)
			return true;
		print_err("select");
	}

	if (ret == 0) {
		fprintf(stderr, "Select timeout\n");
		return false;
	}

	return true;
}
