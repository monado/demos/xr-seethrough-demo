// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: MIT
/*!
 * @brief  NV12 video decoding and lens undistortion shader code.
           Video decoding is based on Nathan Harward's face-responder,
           which is licensed under public domain.
 * @author Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 */

#version 450 core

uniform sampler2D ytex;
uniform sampler2D uvtex;
uniform float texl_w;
uniform vec2 resolution;

layout (location = 2) in vec2 in_uv;

// 480x360
const vec2 f = vec2(222.274576471, 222.274576471);
const vec2 c = vec2(235.451133333, 182.066074435);

const float scale = 0.3;
const vec2 _f = f * scale;

const mat3 K = mat3(
  _f[0], 0,    c[0],
  0,    _f[1], c[1],
  0,    0,     1);

const mat3 iK = inverse(K);

out vec4 color;

// Convert non-linear R'G'B' to linear RGB, taking into account the colorspace.
// V4L2_XFER_FUNC_709
float rgb_to_linear(float value) {
  if (value <= -0.081)
    return -pow((value - 0.099) / -1.099, 1.0 / 0.45);
  else if (value < 0.081)
    return value / 4.5;
  else
    return pow((value + 0.099) / 1.099, 1.0 / 0.45);
}

void main() {

  vec2 pixel_coord = resolution * in_uv;

  // undistort
  vec2 xy = vec2(
    pixel_coord.x * iK[0][0] + pixel_coord.y * iK[0][1] + iK[0][2], 
    pixel_coord.x * iK[1][0] + pixel_coord.y * iK[1][1] + iK[1][2]);

  float r = length(xy);
  
  float scale = (r == 0.0) ? 1.0 : atan(r) / r;
  
  vec2 uv_pixel = f * xy * scale + c;
  vec2 uv = uv_pixel / resolution;
  
  // passthrough undistortion
  // uv_pixel = pixel_coord;
  //uv = in_uv;
  
  float y = texture2D(ytex, uv).r;
  
  // V4L2_PIX_FMT_NV12
  float u, v;
  if (mod(floor(uv_pixel.x), 2.0) == 0.0) {
    u = texture2D(uvtex, uv).r - 0.5;
    v = texture2D(uvtex, vec2(uv.x + texl_w, uv.y)).r - 0.5;
  } else {
    u = texture2D(uvtex, vec2(uv.x - texl_w, uv.y)).r - 0.5;
    v = texture2D(uvtex, uv).r - 0.5;
  }
  
  // Normalize y to [0...1] and uv to [-0.5...0.5], taking into account the
  // colorspace.
  y = (255.0 / 219.0) * (y - (16.0 / 255.0));
  u = (255.0 / 224.0) * u;
  v = (255.0 / 224.0) * v;
  
  // Convert Y'CbCr (aka YUV) to R'G'B', taking into account the
  // colorspace.
  // The HDTV colorspaces all use REC 709 luma coefficients
  float _r = y + 1.5701 * v;
  float _g = y - 0.1870 * u - 0.4664 * v;
  float _b = y + 1.8556 * u;

  if (0 < uv.x && uv.x < 1 && 0 < uv.y && uv.y < 1)
    color = vec4(
      rgb_to_linear(_r),
      rgb_to_linear(_g),
      rgb_to_linear(_b),
      0.0);
  else
    color = vec4(0, 0, 0, 1);
}
