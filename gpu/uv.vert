// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: MIT
/*!
 * @brief  Video seethrough shader code.
 * @author Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 */

#version 450 core

layout (location = 0) in vec2 position;
layout (location = 1) in vec2 uv;
layout (location = 2) out vec2 out_uv;

void main() {
  out_uv = uv;
  gl_Position = vec4(position, 0.0, 1.0);
}

