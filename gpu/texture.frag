// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: MIT
/*!
 * @brief  Video seethrough shader code.
 * @author Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 */

#version 450 core

uniform sampler2D tex;

layout (location = 2) in vec2 in_uv;

out vec4 color;

void main() {
  vec2 uv = vec2(in_uv.x, 1.0 - in_uv.y);
  color = texture2D(tex, uv);
  color.a = 0.3f;
}
