#pragma once

#include <vector>
#include <string>
#include <fstream>

#include <GL/glcorearb.h>
#include "GL/gl3w.h"

#define GLM_ENABLE_EXPERIMENTAL
#define GLM_FORCE_CTOR_INIT
#include <glm/glm.hpp>

namespace vig {
  struct AttributeBuffer {
    const char *name;
    unsigned location;
    unsigned element_size;
    unsigned vector_length;
  };

  class Mesh {
  public:
    unsigned vbo, vao;
    GLsizei index_size;
    unsigned vertex_count;
    unsigned vbo_indices;

    std::vector<AttributeBuffer *> attribute_buffers;

    void append_attribute_buffer(const char *name, uint element_size,
                                 uint vector_length, GLfloat *vertices) {
      AttributeBuffer *attrib_buffer = new AttributeBuffer();

      attrib_buffer->name = name;
      attrib_buffer->element_size = element_size;
      attrib_buffer->vector_length = vector_length;

      glGenBuffers(1, &attrib_buffer->location);

      // printf("generated %s buffer #%d\n", attrib_buffer->name,
      //       attrib_buffer->location);

      glBindBuffer(GL_ARRAY_BUFFER, attrib_buffer->location);

      unsigned size = vertex_count *
          attrib_buffer->vector_length *
          attrib_buffer->element_size;

      glBufferData(GL_ARRAY_BUFFER,
                   static_cast<GLsizeiptr>(size),
                   vertices, GL_STATIC_DRAW);

      attribute_buffers.push_back(attrib_buffer);
    }

    void mesh_bind_shader(GLuint program) {
      glUseProgram(program);

      for (AttributeBuffer *buf : attribute_buffers) {
        // printf("%s: location: %d length: %d size: %u\n", buf->name,
        //       buf->location, buf->vector_length, buf->element_size);

        glBindBuffer(GL_ARRAY_BUFFER, buf->location);

        GLint attrib_location = glGetAttribLocation(program, buf->name);

        if (attrib_location != -1) {
          glVertexAttribPointer(static_cast<GLuint>(attrib_location),
                                static_cast<GLint>(buf->vector_length),
                                GL_FLOAT, GL_FALSE, 0,
                                nullptr);
          glEnableVertexAttribArray(static_cast<GLuint>(attrib_location));
        } else {
          printf("could not find attribute %s in shader.\n", buf->name);
        }
      }
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_indices);
    }

    void bind() {
      glBindVertexArray(vao);
    }

    void unbind() {
      glBindBuffer(GL_ARRAY_BUFFER, 0);
      glBindVertexArray(0);
    }

    void init_uv_plane(GLfloat *vertices, bool flip_uvs = false) {
      glGenVertexArrays(1, &vao);
      bind();
      glGenBuffers(1, &vbo_indices);

      GLfloat uvs[] = {
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0
      };

      GLfloat flipped_uvs[] = {
         0.0, 1.0,
         1.0, 1.0,
         1.0, 0.0,
         0.0, 0.0
      };

      const GLushort indices[] = {0, 1, 2, 3, 0};

      vertex_count = 4;

      append_attribute_buffer("position", sizeof(GLfloat), 2, vertices);
      append_attribute_buffer("uv", sizeof(GLfloat), 2, flip_uvs ? flipped_uvs : uvs);

      // index
      index_size = sizeof(indices);  // /sizeof(indices[0])
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_indices);
      glBufferData(GL_ELEMENT_ARRAY_BUFFER, index_size, indices, GL_STATIC_DRAW);
    }

    void draw() {
      glBindVertexArray(vao);
      glDrawElements(GL_TRIANGLE_STRIP, index_size, GL_UNSIGNED_SHORT, nullptr);
    }

  };

  class Shader {
  public:
    GLuint handle;

    void check_log(GLuint handle, GLenum name, std::string title,
                   void (*iv_fun)(GLuint, GLenum, GLint *)) {
      int success;
      char infoLog[512];
      iv_fun(handle, name, &success);
      if (!success) {
        glGetShaderInfoLog(handle, 512, nullptr, infoLog);
        printf("ERROR: shader: %s:\n%s\n", title.c_str(), infoLog);
      }
    }

    GLuint link_shaders(GLuint vert, GLuint frag) {
      GLuint program = glCreateProgram();
      glAttachShader(program, vert);
      glAttachShader(program, frag);
      glLinkProgram(program);
      check_log(program, GL_LINK_STATUS, "link", glGetProgramiv);
      return program;
    }

    void use() {
      glUseProgram(handle);
    }

    void load_vert_frag(const std::string file_vert,
                        const std::string file_frag) {
      GLuint vert = load_shader(file_vert, GL_VERTEX_SHADER);
      GLuint frag = load_shader(file_frag, GL_FRAGMENT_SHADER);

      handle = link_shaders(vert, frag);

      glDeleteShader(vert);
      glDeleteShader(frag);
    }

    GLuint load_shader(const std::string file_name, GLenum type) {
      std::ifstream shader_stream(file_name);
      std::string shader_string((std::istreambuf_iterator<char>(shader_stream)),
                                std::istreambuf_iterator<char>());

      if (shader_string.empty()) {
        printf("ERROR: could not load %s\n", file_name.c_str());
        return 0;
      }

      const char *shader_char = shader_string.c_str();

      GLuint shader = glCreateShader(type);
      glShaderSource(shader, 1, &shader_char, nullptr);
      glCompileShader(shader);
      check_log(shader, GL_COMPILE_STATUS, file_name, glGetShaderiv);
      return shader;
    }

    void set_f(std::string name, float value) {
      GLint location = glGetUniformLocation(handle, name.c_str());
      glUniform1f(location, value);
    }

    void set_i(std::string name, int value) {
      GLint location = glGetUniformLocation(handle, name.c_str());
      glUniform1i(location, value);
    }

    void set_mat4(std::string name, const glm::mat4& mat) {
      GLint location = glGetUniformLocation(handle, name.c_str());
      glUniformMatrix4fv(location, 1, GL_FALSE, &mat[0][0]);
    }

    void set_vec2(std::string name, const glm::vec2& vec) {
      GLint location = glGetUniformLocation(handle, name.c_str());
      glUniform2fv(location, 1, &vec[0]);
    }
  };

  static void check_error(const char *msg) {
    GLenum err = glGetError();
    if (err) fprintf(stderr, "OpenGL Error 0x%x: %s.\n", err, msg);
  }

  static void set_texture_filter(GLuint handle, GLint mode) {
    glBindTexture(GL_TEXTURE_2D, handle);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mode);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mode);
  }

  static void set_texture_wrap(GLuint handle, GLint mode) {
    glBindTexture(GL_TEXTURE_2D, handle);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, mode);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, mode);
  }

  static void create_fbo(int width, int height, GLuint* fbo, GLuint* color_tex) {
    glGenTextures(1, color_tex);
    glGenFramebuffers(1, fbo);

    glBindTexture(GL_TEXTURE_2D, *color_tex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_INT, nullptr);

    set_texture_wrap(*color_tex, GL_CLAMP_TO_EDGE);
    set_texture_filter(*color_tex, GL_LINEAR);

    glBindFramebuffer(GL_FRAMEBUFFER, *fbo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, *color_tex, 0);

    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

    if (status != GL_FRAMEBUFFER_COMPLETE)
      printf("failed to create fbo %x\n", status);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }

}
